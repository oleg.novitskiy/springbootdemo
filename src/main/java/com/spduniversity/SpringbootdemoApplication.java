package com.spduniversity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@SpringBootApplication
public class SpringbootdemoApplication {

	@RequestMapping("/")
	public String home() {
		return "Spring boot application";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringbootdemoApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(ReservationRepository rr) {
		return args -> {
			Arrays.asList("One,Two,Three".split(",")).forEach(n -> rr.save(new Reservation(n)));

			rr.findAll().forEach(System.out::println);
		};
	}
}

@RepositoryRestResource
interface ReservationRepository extends JpaRepository<Reservation, Long> {}

//@RestController
//class ReservationRestController {
//	@RequestMapping("/reservations")
//	Collection<Reservation> reservations() {
//		return this.reservationRepository.findAll();
//	}
//
//	@Autowired
//	private ReservationRepository reservationRepository;
//}


@Entity
class Reservation {
	@Id
	@GeneratedValue
	private Long id;

	public Reservation() {

	}

	public Reservation(String reservationName) {
		this.reservationName = reservationName;
	}

	private String reservationName;

	public String getReservationName() {
		return reservationName;
	}

	public void setReservationName(String reservationName) {
		this.reservationName = reservationName;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Reservation{" +
				"id=" + id +
				", reservationName='" + reservationName + '\'' +
				'}';
	}
}
